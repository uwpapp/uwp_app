﻿using BLEDevices.Models.DataSchemas;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BLEDevices.Models.DataProviders
{
    public class DeviceService
    {
        private static DeviceService _instance;
        public static DeviceService Instance
        {
            get
            {
                if (_instance == null)
                {
                    _instance = new DeviceService();
                }

                return _instance;
            }
        }

        public List<Device> GetAllDevices()
        {
            using (var db = new DatabaseContext())
            {
                return db.Devices.ToList();
            }
        }

        public Device GetDeviceBy(ulong bluetoothAddress)
        {
            using (var db = new DatabaseContext())
            {
                return db.Devices.FirstOrDefault(d => d.BluetoothAddress == bluetoothAddress);
            }
        }

        public Device Add(Device device)
        {
            using (var db = new DatabaseContext())
            {
                var entity = GetDeviceBy(bluetoothAddress: device.BluetoothAddress);
                if (entity != null)
                {
                    return null;
                }

                device.LastUpdated = DateTimeOffset.UtcNow.ToUnixTimeSeconds();
                db.Devices.Add(device);
                db.SaveChanges();

                return device;
            }
        }

        public Device Update(ulong bluetoothAddress, int newStatus)
        {
            using (var db = new DatabaseContext())
            {
                var entity = GetDeviceBy(bluetoothAddress: bluetoothAddress);
                if (entity != null)
                {
                    entity.Status = newStatus;
                    entity.LastUpdated = DateTimeOffset.UtcNow.ToUnixTimeSeconds();
                }

                return null;
            }
        }
    }
}
