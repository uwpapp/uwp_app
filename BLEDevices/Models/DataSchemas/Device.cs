﻿using SQLite.Net.Attributes;

namespace BLEDevices.Models.DataSchemas
{
    [Table("devices")]
    public class Device
    {
        [PrimaryKey]
        [Column("id")]
        public int Id { get; set; }

        [Column("bluetooth_address")]
        public ulong BluetoothAddress { get; set; }

        [Column("mac_address")]
        public string MacAddress { get; set; }

        [Column("beacon_type")]
        public int BeaconType { get; set; }

        [Column("status")]
        public int Status { get; set; }

        [Column("last_updated")]
        public long LastUpdated { get; set; }
    }
}
