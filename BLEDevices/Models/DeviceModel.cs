﻿using BLEDevices.Common;
using BLEDevices.Utils;
using UniversalBeaconLibrary.Beacon;

namespace BLEDevices.Models
{
    public class DeviceModel : BindableBase
    {
        public ulong BluetoothAddress { get; set; }
        public string MacAddress { get; set; }
        public Beacon.BeaconTypeEnum BeaconType { get; set; }

        private BLEDeviceStatus _status;
        public BLEDeviceStatus Status
        {
            get
            {
                return _status;
            }
            set
            {
                SetProperty(ref _status, value);
                OnPropertyChanged("StatusString");
            }
        }

        public string StatusString
        {
            get
            {
                return this.Status.ToString();
            }
        }

        public string BeaconTypeString
        {
            get
            {
                return this.BeaconType.ToString();
            }
        }

        public DeviceModel() { }

        public DeviceModel(ulong bluetoothAddress, string macAddress, Beacon.BeaconTypeEnum beaconType)
        {
            this.BluetoothAddress = bluetoothAddress;
            this.MacAddress = macAddress;
            this.BeaconType = beaconType;
        }
    }
}
