﻿using BLEDevices.Models.DataSchemas;
using Microsoft.Data.Entity;

namespace BLEDevices.Models
{
    public class DatabaseContext : DbContext
    {
        public DbSet<Device> Devices { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseSqlite("Filename=database.db");
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            // Make SensorId required
            modelBuilder.Entity<Device>()
                .Property(b => b.BluetoothAddress)
                .IsRequired();
        }
    }
}
