using System;
using Microsoft.Data.Entity;
using Microsoft.Data.Entity.Infrastructure;
using Microsoft.Data.Entity.Metadata;
using Microsoft.Data.Entity.Migrations;
using BLEDevices.Models;

namespace BLEDevices.Migrations
{
    [DbContext(typeof(DatabaseContext))]
    [Migration("20160610163918_MyFirstMigration")]
    partial class MyFirstMigration
    {
        protected override void BuildTargetModel(ModelBuilder modelBuilder)
        {
            modelBuilder
                .HasAnnotation("ProductVersion", "7.0.0-rc1-16348");

            modelBuilder.Entity("BLEDevices.Models.DataSchemas.Device", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<int>("BeaconType");

                    b.Property<ulong>("BluetoothAddress");

                    b.Property<long>("LastUpdated");

                    b.Property<string>("MacAddress");

                    b.Property<int>("Status");

                    b.HasKey("Id");
                });
        }
    }
}
