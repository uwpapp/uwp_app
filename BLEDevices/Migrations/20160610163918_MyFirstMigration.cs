using System;
using System.Collections.Generic;
using Microsoft.Data.Entity.Migrations;

namespace BLEDevices.Migrations
{
    public partial class MyFirstMigration : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Device",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("Sqlite:Autoincrement", true),
                    BeaconType = table.Column<int>(nullable: false),
                    BluetoothAddress = table.Column<ulong>(nullable: false),
                    LastUpdated = table.Column<long>(nullable: false),
                    MacAddress = table.Column<string>(nullable: true),
                    Status = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Device", x => x.Id);
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable("Device");
        }
    }
}
