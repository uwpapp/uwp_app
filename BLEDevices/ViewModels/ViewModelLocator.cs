﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BLEDevices.ViewModels
{
    class ViewModelLocator
    {
        private static MainViewModel _mainVM;
        public static MainViewModel MainVM
        {
            get
            {
                if (_mainVM == null)
                {
                    _mainVM = new MainViewModel();
                }

                return _mainVM;
            }
        }
    }
}
