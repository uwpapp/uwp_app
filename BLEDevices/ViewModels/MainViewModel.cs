﻿using System;
using System.Linq;
using System.Collections.ObjectModel;
using BLEDevices.Models;
using BLEDevices.Utils;
using System.Windows.Input;
using BLEDevices.Models.DataProviders;
using BLEDevices.Common;
using Windows.UI.Popups;

namespace BLEDevices.ViewModels
{
    public class MainViewModel : BindableBase
    {
        private BLEDeviecScanService _service = new BLEDeviecScanService();

        private bool _isLoading;
        public bool IsLoading
        {
            get
            {
                return _isLoading;
            }
            private set
            {
                SetProperty(ref _isLoading, value);
            }
        }

        /// <summary>
        /// Device List
        /// </summary>
        private ObservableCollection<DeviceModel> _deviceModels = new ObservableCollection<DeviceModel>();

        public ObservableCollection<DeviceModel> DeviceModels
        {
            get
            {
                return _deviceModels;
            }
            private set
            {
                SetProperty(ref _deviceModels, value);
            }
        }

        // Commands
        private ICommand _cmdStart;
        public ICommand CmdStart
        {
            get
            {
                if (_cmdStart == null)
                {
                    _cmdStart = new RelayCommand(OnStartCommandExecute);
                }

                return _cmdStart;
            }
        }

        private ICommand _cmdStop;
        public ICommand CmdStop
        {
            get
            {
                if (_cmdStop == null)
                {
                    _cmdStop = new RelayCommand(OnStopCommandExecute);
                }

                return _cmdStop;
            }
        }

        public MainViewModel()
        {
            LoadDevicesFromDB();
            _service.Received += OnReceived;
            _service.NoDeviceFound += OnNoDeviceFound;
        }

        private void LoadDevicesFromDB()
        {
            this.DeviceModels.Clear();
            var devices = DeviceService.Instance.GetAllDevices();
            foreach (var device in devices)
            {
                this.DeviceModels.Add(new DeviceModel()
                {
                    BluetoothAddress = device.BluetoothAddress,
                    MacAddress = device.MacAddress,
                    BeaconType = (UniversalBeaconLibrary.Beacon.Beacon.BeaconTypeEnum)device.BeaconType,
                    Status = (BLEDeviceStatus)device.Status,
                });
            }
        }

        private async void OnStartCommandExecute(object obj)
        {
            var isBluetoothOn = await ConnectivityUtils.IsBluetoothConnected();
            if (isBluetoothOn)
            {
                _service.StartScanning();
                this.IsLoading = true;
            } 
            else
            {
                MessageDialog dialog = new MessageDialog("Please turn on Bluetooth on your device.", "Oops...");
                await dialog.ShowAsync();
            }
        }

        private void OnStopCommandExecute(object obj)
        {
            _service.StopScanning();
            this.IsLoading = false;
        }

        private void OnReceived(object sender, OnReceiveBLEDeviceArgs e)
        {
            this.IsLoading = false;

            var device = this.DeviceModels.FirstOrDefault(d => d.BluetoothAddress == e.Device.BluetoothAddress);
            if (device != null)
            {
                device.Status = e.Device.Status;
                DeviceService.Instance.Update(device.BluetoothAddress, (int)device.Status);
            }
            else
            {
                device = new DeviceModel(e.Device.BluetoothAddress, e.Device.MacAddress, e.Device.BeaconType);
                device.Status = e.Device.Status;
                this.DeviceModels.Add(device);

                DeviceService.Instance.Add(new Models.DataSchemas.Device()
                {
                    BluetoothAddress = device.BluetoothAddress,
                    MacAddress = device.MacAddress,
                    BeaconType = (int)device.BeaconType,
                    Status = (int)device.Status
                });
            }
        }

        private async void OnNoDeviceFound(object sender, EventArgs e)
        {
            _service.StopScanning();
            this.IsLoading = false;

            MessageDialog dialog = new MessageDialog("Sorry, cannot find any devices around here.", "Oops...");
            await dialog.ShowAsync(); ;
        }
    }
}
