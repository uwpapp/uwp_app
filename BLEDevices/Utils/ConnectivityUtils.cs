﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.Networking.Connectivity;
using Windows.Networking.Proximity;

namespace BLEDevices.Utils
{
    public static class ConnectivityUtils
    {
        public static bool HasInternetConnection()
        {
            var connections = NetworkInformation.GetConnectionProfiles().ToList();
            connections.Add(NetworkInformation.GetInternetConnectionProfile());

            foreach (var connection in connections)
            {
                if (connection == null)
                    continue;

                if (connection.GetNetworkConnectivityLevel() == NetworkConnectivityLevel.InternetAccess)
                {
                    return true;
                }
            }

            return false;
        }

        public static async Task<bool> IsBluetoothConnected()
        {
            PeerFinder.Start();

            try
            {
                PeerFinder.AlternateIdentities["Bluetooth:Paired"] = "";
                var peers = await PeerFinder.FindAllPeersAsync();
            }
            catch (Exception ex)
            {
                if ((uint)ex.HResult == 0x8007048F)
                {
                    return false;
                }
            }

            return true;
        }
    }
}
