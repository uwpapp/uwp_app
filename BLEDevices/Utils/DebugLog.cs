﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BLEDevices.Utils
{
    public static class DebugLog
    {
        public static void LogError(string message)
        {
            Debug.WriteLine("[Error] " + message.Trim());
        }

        public static void LogInfo(string message)
        {
            Debug.WriteLine("[Debug] " + message.Trim());
        }
    }
}
