﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using UniversalBeaconLibrary.Beacon;
using Windows.Devices.Bluetooth.Advertisement;
using Windows.Storage.Streams;
using Windows.UI.Xaml;

namespace BLEDevices.Utils
{
    /// <summary>
    /// Status of BLE device
    /// </summary>
    public enum BLEDeviceStatus
    {
        Unavailable = 0,
        Available,
        New = 1
    }

    /// <summary>
    /// Represent BLE device
    /// </summary>
    public class BLEDevice
    {
        public ulong BluetoothAddress { get; set; }
        public string MacAddress { get; set; }
        public BLEDeviceStatus Status { get; set; }
        public Beacon.BeaconTypeEnum BeaconType { get; set; }
    }

    public class OnReceiveBLEDeviceArgs : EventArgs
    {
        public BLEDevice Device { get; }

        public OnReceiveBLEDeviceArgs(BLEDevice device)
        {
            this.Device = device;
        }
    }

    public class BLEDeviecScanService
    {
        private BluetoothLEAdvertisementWatcher _watcher;
        private bool _isRunning = false;
        private List<BLEDevice> _devices = new List<BLEDevice>();
        private BeaconManager _beaconManager = new BeaconManager();
        private DispatcherTimer _timer;

        public event EventHandler<OnReceiveBLEDeviceArgs> Received;
        public event EventHandler NoDeviceFound;

        public BLEDeviecScanService(int timeOut = 30)
        {
            _watcher = new BluetoothLEAdvertisementWatcher();
            _watcher.Received += Watcher_Received;
            _watcher.Stopped += Watcher_Stopped;

            _watcher.SignalStrengthFilter.OutOfRangeTimeout = TimeSpan.FromMilliseconds(1000);
            _watcher.SignalStrengthFilter.SamplingInterval = TimeSpan.FromMilliseconds(0);

            _timer = new DispatcherTimer()
            {
                Interval = TimeSpan.FromSeconds(timeOut)
            };
            _timer.Tick += OnTimerTick;
        }

        private void OnTimerTick(object sender, object e)
        {
            if (_devices.Count < 1)
            {
                if (NoDeviceFound != null)
                {
                    NoDeviceFound(this, EventArgs.Empty);
                }
            }

            _timer.Stop();
        }

        public void StartScanning()
        {
            if (_isRunning)
            {
                return;
            }

            _isRunning = true;
            _watcher.Start();
            _timer.Start();
            _devices.Clear();
            _beaconManager.BluetoothBeacons.Clear();
        }

        public void StopScanning()
        {
            if (_isRunning)
            {
                _watcher.Stop();
            }

            _timer.Stop();
        }

        private void OnReceived(BLEDevice device)
        {
            if (Received != null)
            {
                var eventArgs = new OnReceiveBLEDeviceArgs(device);
                Received(this, eventArgs);
            }
        }

        #region Watcher Events

        private void Watcher_Stopped(BluetoothLEAdvertisementWatcher sender, BluetoothLEAdvertisementWatcherStoppedEventArgs args)
        {
            _isRunning = false;
        }

        /// <summary>
        /// On received advertisement data from nearby devices
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="args"></param>
        private void Watcher_Received(BluetoothLEAdvertisementWatcher sender, BluetoothLEAdvertisementReceivedEventArgs args)
        {
            _beaconManager.ReceivedAdvertisement(args);

            // Find beacon with bluetooh address in Beacon Manager
            var beacon = _beaconManager.BluetoothBeacons.FirstOrDefault(b => b.BluetoothAddress == args.BluetoothAddress &&
                b.BeaconType != Beacon.BeaconTypeEnum.Unknown);
            if (beacon != null)
            {
                var device = _devices.FirstOrDefault(d => d.BluetoothAddress == beacon.BluetoothAddress);
                if (device == null)
                {
                    // New device detected
                    device = new BLEDevice();
                    device.BluetoothAddress = beacon.BluetoothAddress;
                    device.MacAddress = beacon.BluetoothAddressAsString;
                    device.BeaconType = beacon.BeaconType;
                    device.Status = beacon.Rssi < -127 ? BLEDeviceStatus.Unavailable : BLEDeviceStatus.New;
                }
                else
                {
                    // Status updated for a known device
                    device.Status = beacon.Rssi < -127 ? BLEDeviceStatus.Unavailable : BLEDeviceStatus.Available;
                }

                OnReceived(device);
            }
        }

        #endregion
    }
}