﻿using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.Windows.Threading;

namespace BLEDevices.Common
{
    public abstract class BindableBase : INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;

        protected bool SetProperty<T>(ref T storage, T value, [CallerMemberName] string propertyName = null)
        {
            if (Equals(storage, value))
            {
                return false;
            }

            storage = value;
            this.OnPropertyChanged(propertyName);
            return true;
        }

        protected void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChangedEventHandler eventHandler = this.PropertyChanged;
            if (eventHandler != null)
            {
                SmartDispatcher.BeginInvoke(delegate
                {
                    eventHandler(this, new PropertyChangedEventArgs(propertyName));
                });
            }
        }
    }
}
